#ifndef WCC
#include "support.h"
#else
/* Configure the required attribute for benchmark execution section.  */
#ifdef RAMEXEC_SECTION
/* FORNOW use a constant name.  */
#define EXEC_SECTION_ATTRIBUTE __attribute__((section(".ramexecute")))
#else
/* No RAM-execute section specified.  */
#define EXEC_SECTION_ATTRIBUTE
#endif
#endif

static volatile unsigned int counter = 0x100000;

EXEC_SECTION ATTRIBUTE
int benchmark (void)
{
  counter = 0x1000;
  while (counter != 0)
	  counter-- ;

  return 0;
}
