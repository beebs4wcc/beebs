import matplotlib.lines as mlines
import matplotlib.pyplot as plt
import numpy as np
import csv

# Proxy artists for legend layout.
# Note the colour matching the 'y' ('yellow') of plot is actually XKCD 'dirty yellow'.  This colour
# offers better contrast on a white background.
r_o = mlines.Line2D([], [], color='red', marker='o', markersize=5, label='Flash, prefetch OFF, 0 wait states')	   
y_o = mlines.Line2D([], [], color='xkcd:dirty yellow', marker='o', markersize=5, label='Flash, prefetch OFF, 1 wait state')
b_o = mlines.Line2D([], [], color='blue', marker='o', markersize=5, label='Flash, prefetch ON, 0 wait states')	   
g_o = mlines.Line2D([], [], color='green', marker='o', markersize=5, label='Flash, prefetch ON, 1 wait state')

r_v = mlines.Line2D([], [], color='red', marker='^', markersize=5, label='RAM, prefetch OFF, 0 wait states')
y_v = mlines.Line2D([], [], color='xkcd:dirty yellow', marker='^', markersize=5, label='RAM, prefetch OFF, 1 wait state')
b_v = mlines.Line2D([], [], color='blue', marker='^', markersize=5, label='RAM, prefetch ON, 0 wait states')
g_v = mlines.Line2D([], [], color='green', marker='^', markersize=5, label='RAM, prefetch ON, 1 wait state')


class Result(object):
    ''' A result is an entry in the result list.
        It consists of the following fields:
        - 0: directory (str)
        - 1: benchmark (str)
        - 2: mapping mode (str, either 'flash' or 'ramexecute')
        - 3: frequency (int)
        - 4: prefetch mode (int, either 0 meaning OFF, or 1 meaning ON)
        - 5: number of wait states (int)
        - 6: total energy (float)
        - 7: total time (float)
        - 8: average power (float)
        - 9: average current (float)
        - 10: average voltage (float)
        '''

    def __init__(self, l):
        ''' Create a result record from a list of values.
        The prefix fields (up to 11 elements) must be present.
        All excess fields will be ignored. '''
        self.dir = l[0].strip() if len(l) > 0 else None
        self.bench = l[1].strip() if len(l) > 1 else None
        self.mode = l[2].strip() if len(l) > 2 else None
        self.frequency = int(l[3]) if len(l) > 3 else None
        self.prefetch = int(l[4]) if len(l) > 4 else None
        self.waitstates = int(l[5]) if len(l) > 5 else None
        self.energy = float(l[6]) if len(l) > 6 and l[6].strip() != '' else None
        self.duration = float(l[7]) if len(l) > 7 and l[7].strip() != '' else None
        self.avgpower = float(l[8]) if len(l) > 8 and l[8].strip() != '' else None
        self.avgcurrent = float(l[9]) if len(l) > 9 and l[9].strip() != '' else None
        self.avgvoltage = float(l[10]) if len(l) > 10 and l[10].strip() != '' else None

    def __list__(self):
        ''' List representation of a result. '''
        return [self.dir, self.bench, self.mode, self.frequency, self.prefetch, self.waitstates, self.energy, self.duration,
                self.avgpower, self.avgcurrent, self.avgvoltage]

    def __str__(self):
        ''' String representation of a result. '''
        return '<class Result({}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {})>'.format(
            self.dir, self.bench, self.mode, self.frequency, self.prefetch, self.waitstates,
            self.energy, self.duration, self.avgpower, self.avgcurrent, self.avgvoltage)


def sort_key_freq(record):
    ''' Generate a frequency-based sorting key from current result record. '''
    if isinstance(record, list):
        # Unconverted list record: make sure the frequency files (index 3) is there.
        # Assume missing frequency to mean sort-as-first.
        return int(record[3]) if len(record) > 3 else 0
    elif isinstance(record, Result):
        return record.frequency
    else:
        return None


def load_data(fname):
    ''' Load the result data from a comma-separated CSV file.
    Organise data according to mapping mode, prefetch and wait state setting, sorting by frequency in the process. '''
    result_data = {'flash': [[[], []], [[], []]],
                   'ramexecute': [[[], []], [[], []]]}
    with open(fname, 'r') as f:
        result_reader = csv.reader(f, delimiter=',')
        rows = list(result_reader)

    # print('First row of data: ' + str(rows[0]))

    # Sort and classify data selecting rows that match
    # the given mapping/prefetch/WS setting.
    for mode in ['flash', 'ramexecute']:
        for pf in range(2):
            for ws in range(2):
                result_data[mode][pf][ws] = sorted(
                    [Result(row) for row in rows if
                        row[2].strip() == mode and
                        row[4].strip() == str(pf) and
                        row[5].strip() == str(ws)], key=sort_key_freq)
                #print([Result(row) for row in rows])
                #print(result_data[mode][pf][ws])

    return result_data



def power(record):
    ''' Return the average power for the given record. '''
    return record.avgpower


def mW_per_MHz(record):
    ''' Return the unit power-per-MHz for current record in milliWatt. '''
    return record.avgpower * 1000000000.0 / record.frequency


def mA(record):
    ''' Return the average current for given record in mA. '''
    return record.avgcurrent * 1000.0


def energy(record):
    ''' Return the total energy for given record.  '''
    return record.energy


def frequency(record):
    ''' Return the frequency for the given record. '''
    return record.frequency


def frequency_MHz(record):
    ''' Return the frequency in MHz for the given record. '''
    return record.frequency/1000000


def duration(record):
    ''' Return the duration of the given record. '''
    return record.duration


def scatter_feature_against_feature(result_data, xfeature, yfeature, bench, title, xlabel, ylabel, legend_loc=None):
    ''' Prepare a multi-dataset scatter plot of two selected features. '''

    # Initialise the value lists.
    xvalues = {'flash': [[[], []], [[], []]], 'ramexecute': [[[], []], [[], []]]}
    yvalues = {'flash': [[[], []], [[], []]], 'ramexecute': [[[], []], [[], []]]}

    # Set up symbols, labels, and colours.
    symbol = {'flash': 'o', 'ramexecute': 'v'}
    mode_label = {'flash': 'Flash', 'ramexecute': 'RAM'}
    prefetch_label = ['OFF', 'ON']
    colour = [['r', 'y'], ['b', 'g']]

    # Build the NumPy arrays of the relevant data values.
    for mode in ['flash', 'ramexecute']:
        for pf in range(2):
            for ws in range(2):
                xvalues[mode][pf][ws] = np.array([xfeature(x) for x in result_data[mode][pf][ws] if x.bench == bench and x.mode == mode])
                yvalues[mode][pf][ws] = np.array([yfeature(y) for y in result_data[mode][pf][ws] if y.bench == bench and y.mode == mode])

    # Do not attempt to plot if the X and Y axis arrays are not the same length.
    if len(xvalues) > 0 and len(xvalues) == len(yvalues):
        # Change axis scale to logarithmic for frequency (either axis).
        fig = plt.figure()
        ax = fig.add_subplot(2,1,1)
        if xfeature == frequency or xfeature == frequency_MHz:
            ax.set_xscale('log')
        if yfeature == frequency or yfeature == frequency_MHz:
            ax.set_yscale('log')

        # Set title.
        plt.title('{} in benchmark {}'.format(title, bench))

        # For MHz frequencies use increments of 4 MHz
        if xfeature == frequency_MHz:
            plt.xticks(np.arange(8, 52, 4))
        # Enable automatic grid.
        plt.grid(True)

        # Plot data points for all memory mappings, prefetch modes and wait state counts.
        for mode in ['flash', 'ramexecute']:
            for pf in range(2):
                for ws in range(2):
                    plt.scatter(xvalues[mode][pf][ws], yvalues[mode][pf][ws],
                                c=colour[pf][ws], marker=symbol[mode],
                                label='{}, prefetch {}, WS {}'.format(mode_label[mode], prefetch_label[pf], ws))
        # Override legend placement if necessary.
        if legend_loc is not None:
            plt.legend(loc=legend_loc, framealpha=0.35)
        else:
            plt.legend(loc='best', framealpha=0.35)
        # Add axis labels.
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        #plt.show()
    else:
        print('*** Incomplete data, cannot plot!')


def plot_freq_feature(result_data, yfeature, bench, title, ylabel, legend_loc=None):
    ''' Plot the specified feature of result data.  '''
    scatter_feature_against_feature(result_data, frequency_MHz, yfeature, bench, title, 'Frequency [MHz]', ylabel, legend_loc)
    plt.show()


if __name__ == '__main__':
    import sys
    results = load_data(sys.argv[1])
    # Select all benchmarks for which the results at 8MHz Flash, PF OFF, WS=0 are available.
    benchmarks = sorted(list(set([result.bench for result in results['flash'][0][0] if result.frequency == 8000000])))

    # Set figure size to 1280x1024 pixels, use default DPI setting to convert
    # pixels to inches (which are the default size unit in pyplot.)
    dpi = plt.rcParams['figure.dpi']
    plt.rcParams['figure.figsize'] = 1280/dpi, 720/dpi

    for bench in benchmarks:
        try:
            # Duration:energy data
            scatter_feature_against_feature(results, duration, energy, bench, 'Energy', 'Duration [s]', 'Energy [J]')
            plt.savefig('{}/{}-duration-energy.svg'.format(sys.argv[2], bench))
            plt.clf()
        except:
            print('Plotting failed for bench {}, mode {}!'.format(bench, 'duration-energy'))

        try:
            # Frequency:energy data
            scatter_feature_against_feature(results, frequency_MHz, energy, bench, 'Energy', 'Frequency [MHz]', 'Energy [J]')
            plt.savefig('{}/{}-frequency-energy.svg'.format(sys.argv[2], bench))
            plt.clf()
        except:
            print('Plotting failed for bench {}, mode {}!'.format(bench, 'frequency-energy'))

        try:
            # Frequency:unit_power data
            scatter_feature_against_feature(results, frequency_MHz, mW_per_MHz, bench, 'Unit power', 'Frequency [MHz]', 'Unit power [mW/MHz]')
            plt.savefig('{}/{}-frequency-unitpower.svg'.format(sys.argv[2], bench))
            plt.clf()
        except:
            print('Plotting failed for bench {}, mode {}!'.format(bench, 'frequency-energy'))

        try:
            # Duration:unit_power data
            scatter_feature_against_feature(results, duration, mW_per_MHz, bench, 'Unit power', 'Duration [s]', 'Unit power [mW/MHz]')
            plt.savefig('{}/{}-duration-unitpower.svg'.format(sys.argv[2], bench))
            plt.clf()
        except:
            print('Plotting failed for bench {}, mode {}!'.format(bench, 'frequency-energy'))

        try:
            # Frequency:duration data
            scatter_feature_against_feature(results, frequency_MHz, duration, bench, 'Duration', 'Frequency [MHz]', 'Duration [s]')
            plt.savefig('{}/{}-frequency-duration.svg'.format(sys.argv[2], bench))
            plt.clf()
        except:
            print('Plotting failed for bench {}, mode {}!'.format(bench, 'frequency-duration'))
