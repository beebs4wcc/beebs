/* Copyright (C) 2014-2019 Embecosm Limited and University of Bristol

   Contributor James Pallister <james.pallister@bristol.ac.uk>

   This file is part of the Bristol/Embecosm Embedded Benchmark Suite.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include <support.h>

void initialise_board()
{
#ifndef THUMBULATOR
    volatile int i;

    PIN_INIT(0);
    chip_set_timings (chip_clock_frequency, chip_prefetch_enable, chip_flash_ws);

    /* Enable MCO (Microcontroller Clock Output) using alternate function 0
       (AF0) of pin PA8, cf. STM32F051x{6,8} datasheet, ST document stm32f051t8.pdf,
       table 14 p. 37.  */
    /* Turn on GPIO A Clock */
    RCC_AHBENR |= RCC_AHBENR_GPIOAEN;

    /* Set A8 mode to alternate function.  */
    GPIOA_MODER = (GPIOA_MODER & ~GPIO_MODE_MASK(8)) | GPIO_MODE(8, GPIO_MODE_AF);
    /* AF0 is the default at reset.  */

    /* Run a quick busyloop to catch the configured clock.  */
    for (i = 100; i > 0; i--)
    ;

    /* Switch back pin A8 to plain input mode.  */
    GPIOA_MODER = (GPIOA_MODER & ~GPIO_MODE_MASK(8)) | GPIO_MODE(8, GPIO_MODE_INPUT);

    /* Disable clock of the entire GPIO port A.  */
    RCC_AHBENR ^= RCC_AHBENR_GPIOAEN;
#endif
}

EXEC_SECTION_ATTRIBUTE
void start_trigger()
{
    PIN_SET(0);
}

EXEC_SECTION_ATTRIBUTE
void stop_trigger()
{
    PIN_CLEAR(0);
}
