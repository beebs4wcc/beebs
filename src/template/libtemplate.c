

/* -*- mode: C++; c-file-style: "gnu-mode" -*- */
/* BEEBS template benchmark

   *** Put any preceding copyright here ***
   Copyright (C) 2013 Embecosm Limited and University of Bristol

   Contributor Jeremy Bennett <jeremy.bennett@embecosm.com>

   This file is part of the Bristol/Embecosm Embedded Benchmark Suite.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef WCC
#include "support.h"
#else
/* Configure the required attribute for benchmark execution section.  */
#ifdef RAMEXEC_SECTION
/* FORNOW use a constant name.  */
#define EXEC_SECTION_ATTRIBUTE __attribute__((section(".ramexecute")))
#else
/* No RAM-execute section specified.  */
#define EXEC_SECTION_ATTRIBUTE
#endif
#endif

/* This scale factor will be changed to equalise the runtime of the
   benchmarks. */
#define SCALE_FACTOR    (REPEAT_FACTOR >> 0)




/* This benchmark does not support verification */

EXEC_SECTION_ATTRIBUTE
int
#ifndef WCC
verify_benchmark (int res __attribute ((unused)) )
#else
verify_benchmark (int res)
#endif
{
  return -1;
}


EXEC_SECTION_ATTRIBUTE
void
initialise_benchmark (void)
{
}


EXEC_SECTION_ATTRIBUTE
int
benchmark (void)
{
  /* Code to benchmark goes here */
  return 0;
}


