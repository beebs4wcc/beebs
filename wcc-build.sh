#!/bin/bash

DEFAULT_LIST="bs,bubblesort,cnt,cover,crc,crc32,duff,expint,fac,fibcall,fir,insertsort,janne_complex,jfdctint,lcdnum,ludcmp,minver,ndes,nsichneu,prime,qsort,qurt,recursion,select,sqrt,statemate,strstr,tarai,template,ud"

SUMMARY_MSG="\n\n#######\nSummary\n#######\n\n"

EXPERIMENTAL_LIST=""
scriptdir="$(dirname "$(readlink -f "$0")")"

function build-default {
  IFS=,
  for val in $DEFAULT_LIST; do
    bm="./src/$val"
    cd $bm && make -f Makefile.wcc
    rv=$?
    if [ $rv -ne 0 ]; then
      SUMMARY_MSG+="$bm : ERROR\n"
    else
      var_count=$(ls CodeVariants | wc -l)
      SUMMARY_MSG+="$bm : SUCCESS, $var_count variants\n"
    fi
    cd $scriptdir
  done
}

if [ $1 = "clean" ]; then
  IFS=,
  for val in $DEFAULT_LIST; do
    bm="./src/$val"
    echo "Cleaning $bm..."
    cd $bm && make -f Makefile.wcc clean
    cd $scriptdir
  done
  SUMMARY_MSG+="All cleaned\n"
else
  build-default
fi
printf "$SUMMARY_MSG\n"
