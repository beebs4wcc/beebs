/* Chip support for NXP's LPC11Cxx / LPC11xx Cortex-M0 SoCs.
   Copyright (C) 2014 Embecosm Limited and the University of Bristol

   Contributor Zbigniew Chamski <zbigniew.chamski@gmail.com>

   This file is part of BEEBS

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the Free
   Software Foundation; either version 3 of the License, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.  */

#ifndef CHIPSUPPORT_H
#define CHIPSUPPORT_H

/* Wait-for-interrupt instruction, library-independent definition.   */
__attribute__( ( always_inline ) ) static inline void __WFI (void)
{
	__asm volatile ("wfi");
}

// Define the registers we need to do a pin toggle
#define REG_AT_INDEX(base, index)	(((unsigned int *)(base))[(index)])

// Base of the system control block.
#define LPC_SYSCTL_BASE	0x40048000

// Index of the SYSAHBCLKCTRL register in LPC_SYSCTL reg array.
#define SYSAHBCLKCTRL_INDEX	(0x80 >> 2)

// Base of the GPIO state space.
#define LPC_GPIO_BASE	0x50000000

// Base of the GPIO control area.
#define LPC_IOCON_BASE	0x40044000

// Beebs signalling pins are by default 3.0, 3.1 and 3.2
#define PIN_IOCON_OFFSET(pin)		__PIN_IOCON_OFFSET (pin)
#define __PIN_IOCON_OFFSET(pin)		IOCON_OFFSET_3_ ## pin

#define IOCON_OFFSET_0_0 (0x00C >> 2)
#define IOCON_OFFSET_0_1 (0x010 >> 2)
#define IOCON_OFFSET_0_2 (0x01C >> 2)
#define IOCON_OFFSET_0_3 (0x02C >> 2)
#define IOCON_OFFSET_0_4 (0x030 >> 2)
#define IOCON_OFFSET_0_5 (0x034 >> 2)
#define IOCON_OFFSET_0_6 (0x04C >> 2)
#define IOCON_OFFSET_0_7 (0x050 >> 2)
#define IOCON_OFFSET_0_8 (0x060 >> 2)
#define IOCON_OFFSET_0_9 (0x064 >> 2)
#define IOCON_OFFSET_0_10 (0x070 >> 2)
#define IOCON_OFFSET_0_11 (0x074 >> 2)

#define IOCON_OFFSET_1_0 (0x078 >> 2)
#define IOCON_OFFSET_1_1 (0x07C >> 2)
#define IOCON_OFFSET_1_2 (0x080 >> 2)
#define IOCON_OFFSET_1_3 (0x090 >> 2)
#define IOCON_OFFSET_1_4 (0x094 >> 2)
#define IOCON_OFFSET_1_5 (0x0A0 >> 2)
#define IOCON_OFFSET_1_6 (0x0A4 >> 2)
#define IOCON_OFFSET_1_7 (0x0A8 >> 2)
#define IOCON_OFFSET_1_8 (0x014 >> 2)
#define IOCON_OFFSET_1_9 (0x038 >> 2)
#define IOCON_OFFSET_1_10 (0x06C >> 2)
#define IOCON_OFFSET_1_11 (0x098 >> 2)

#define IOCON_OFFSET_2_0 (0x008 >> 2)
#define IOCON_OFFSET_2_1 (0x028 >> 2)
#define IOCON_OFFSET_2_2 (0x05C >> 2)
#define IOCON_OFFSET_2_3 (0x08C >> 2)
#define IOCON_OFFSET_2_4 (0x040 >> 2)
#define IOCON_OFFSET_2_5 (0x044 >> 2)
#define IOCON_OFFSET_2_6 (0x000 >> 2)
#define IOCON_OFFSET_2_7 (0x020 >> 2)
#define IOCON_OFFSET_2_8 (0x024 >> 2)
#define IOCON_OFFSET_2_9 (0x054 >> 2)
#define IOCON_OFFSET_2_10 (0x058 >> 2)
#if !defined(CHIP_LPC1125)
#define IOCON_OFFSET_2_11 (0x070 >> 2)
#endif

#define IOCON_OFFSET_3_0 (0x084 >> 2)
#if !defined(CHIP_LPC1125)
#define IOCON_OFFSET_3_1 (0x088 >> 2)
#endif
#define IOCON_OFFSET_3_2 (0x09C >> 2)
#define IOCON_OFFSET_3_3 (0x0AC >> 2)
#define IOCON_OFFSET_3_4 (0x03C >> 2)
#define IOCON_OFFSET_3_5 (0x048 >> 2)

#define IOCON_OFFSET_LED		IOCON_OFFSET_0_7

#define MODE_FLOAT			(0 << 3)
#define MODE_PULLDOWN			(1 << 3)

#define SET_PIN_AS_OUTPUT(PORT, PIN) \
  do {								\
    /* Set pin direction to output.  The DIR word is preceded by 8192 regs.  */	\
    REG_AT_INDEX (LPC_GPIO_BASE + (PORT) * 0x10000, 8192) |= (1UL << (PIN));	\
  } while (0)

#define CLEAR_GPIO_PIN(PORT, PIN) \
   do {										\
     /* Clear pin value.  */							\
     REG_AT_INDEX (LPC_GPIO_BASE + (PORT) * 0x10000, 1 << (PIN)) = 0UL << (PIN);\
   } while (0)


/*  peripherals altogether to get a base consumption figure.
    BEWARE: pins 2.11 and 3.1 do not exist on LPC1125.

    !!! IMPORTANT !!!

    Do not pull down pin 1.3 - this is the SWDIO debug line which should be left
    PULLED UP (this is its default reset PU-PD mode.)  */
#ifdef CHIP_LPC1125
#error [DISABLE_PERIPHERALS] Please handle the absence of pins 2.11 and 3.1 on LPC1125
#endif

#define DISABLE_PERIPHERALS() \
  do {									\
    SET_PIN_AS_OUTPUT (0, 0);						\
    SET_PIN_AS_OUTPUT (0, 1);						\
    SET_PIN_AS_OUTPUT (0, 2);						\
    SET_PIN_AS_OUTPUT (0, 3);						\
    SET_PIN_AS_OUTPUT (0, 4);						\
    SET_PIN_AS_OUTPUT (0, 5);						\
    SET_PIN_AS_OUTPUT (0, 6);						\
    SET_PIN_AS_OUTPUT (0, 7);						\
    SET_PIN_AS_OUTPUT (0, 8);						\
    SET_PIN_AS_OUTPUT (0, 9);						\
    SET_PIN_AS_OUTPUT (0, 10);						\
    SET_PIN_AS_OUTPUT (0, 11);						\
    CLEAR_GPIO_PIN (0, 0);						\
    CLEAR_GPIO_PIN (0, 1);						\
    CLEAR_GPIO_PIN (0, 2);						\
    CLEAR_GPIO_PIN (0, 3);						\
    CLEAR_GPIO_PIN (0, 4);						\
    CLEAR_GPIO_PIN (0, 5);						\
    CLEAR_GPIO_PIN (0, 6);						\
    CLEAR_GPIO_PIN (0, 7);						\
    CLEAR_GPIO_PIN (0, 8);						\
    CLEAR_GPIO_PIN (0, 9);						\
    CLEAR_GPIO_PIN (0, 10);						\
    CLEAR_GPIO_PIN (0, 11);						\
    REG_AT_INDEX (LPC_IOCON_BASE, IOCON_OFFSET_0_0) = MODE_FLOAT;	\
    REG_AT_INDEX (LPC_IOCON_BASE, IOCON_OFFSET_0_1) = MODE_FLOAT;	\
    REG_AT_INDEX (LPC_IOCON_BASE, IOCON_OFFSET_0_2) = MODE_FLOAT;	\
    REG_AT_INDEX (LPC_IOCON_BASE, IOCON_OFFSET_0_3) = MODE_FLOAT;	\
    REG_AT_INDEX (LPC_IOCON_BASE, IOCON_OFFSET_0_4) = MODE_FLOAT;	\
    REG_AT_INDEX (LPC_IOCON_BASE, IOCON_OFFSET_0_5) = MODE_FLOAT;	\
    REG_AT_INDEX (LPC_IOCON_BASE, IOCON_OFFSET_0_6) = MODE_FLOAT;	\
    REG_AT_INDEX (LPC_IOCON_BASE, IOCON_OFFSET_0_7) = MODE_FLOAT;	\
    REG_AT_INDEX (LPC_IOCON_BASE, IOCON_OFFSET_0_8) = MODE_FLOAT;	\
    REG_AT_INDEX (LPC_IOCON_BASE, IOCON_OFFSET_0_9) = MODE_FLOAT;	\
    REG_AT_INDEX (LPC_IOCON_BASE, IOCON_OFFSET_0_10) = MODE_FLOAT;	\
    REG_AT_INDEX (LPC_IOCON_BASE, IOCON_OFFSET_0_11) = MODE_FLOAT;	\
    SET_PIN_AS_OUTPUT (1, 0);						\
    SET_PIN_AS_OUTPUT (1, 1);						\
    SET_PIN_AS_OUTPUT (1, 2);						\
    SET_PIN_AS_OUTPUT (1, 4);						\
    SET_PIN_AS_OUTPUT (1, 5);						\
    SET_PIN_AS_OUTPUT (1, 6);						\
    SET_PIN_AS_OUTPUT (1, 7);						\
    SET_PIN_AS_OUTPUT (1, 8);						\
    SET_PIN_AS_OUTPUT (1, 9);						\
    SET_PIN_AS_OUTPUT (1, 10);						\
    SET_PIN_AS_OUTPUT (1, 11);						\
    CLEAR_GPIO_PIN (1, 0);						\
    CLEAR_GPIO_PIN (1, 1);						\
    CLEAR_GPIO_PIN (1, 2);						\
    CLEAR_GPIO_PIN (1, 4);						\
    CLEAR_GPIO_PIN (1, 5);						\
    CLEAR_GPIO_PIN (1, 6);						\
    CLEAR_GPIO_PIN (1, 7);						\
    CLEAR_GPIO_PIN (1, 8);						\
    CLEAR_GPIO_PIN (1, 9);						\
    CLEAR_GPIO_PIN (1, 10);						\
    CLEAR_GPIO_PIN (1, 11);						\
    REG_AT_INDEX (LPC_IOCON_BASE, IOCON_OFFSET_1_0) = MODE_FLOAT;	\
    REG_AT_INDEX (LPC_IOCON_BASE, IOCON_OFFSET_1_1) = MODE_FLOAT;	\
    REG_AT_INDEX (LPC_IOCON_BASE, IOCON_OFFSET_1_2) = MODE_FLOAT;	\
    REG_AT_INDEX (LPC_IOCON_BASE, IOCON_OFFSET_1_4) = MODE_FLOAT;	\
    REG_AT_INDEX (LPC_IOCON_BASE, IOCON_OFFSET_1_5) = MODE_FLOAT;	\
    REG_AT_INDEX (LPC_IOCON_BASE, IOCON_OFFSET_1_6) = MODE_FLOAT;	\
    REG_AT_INDEX (LPC_IOCON_BASE, IOCON_OFFSET_1_7) = MODE_FLOAT;	\
    REG_AT_INDEX (LPC_IOCON_BASE, IOCON_OFFSET_1_8) = MODE_FLOAT;	\
    REG_AT_INDEX (LPC_IOCON_BASE, IOCON_OFFSET_1_9) = MODE_FLOAT;	\
    REG_AT_INDEX (LPC_IOCON_BASE, IOCON_OFFSET_1_10) = MODE_FLOAT;	\
    REG_AT_INDEX (LPC_IOCON_BASE, IOCON_OFFSET_1_11) = MODE_FLOAT;	\
    SET_PIN_AS_OUTPUT (2, 0);						\
    SET_PIN_AS_OUTPUT (2, 1);						\
    SET_PIN_AS_OUTPUT (2, 2);						\
    SET_PIN_AS_OUTPUT (2, 3);						\
    SET_PIN_AS_OUTPUT (2, 4);						\
    SET_PIN_AS_OUTPUT (2, 5);						\
    SET_PIN_AS_OUTPUT (2, 6);						\
    SET_PIN_AS_OUTPUT (2, 7);						\
    SET_PIN_AS_OUTPUT (2, 8);						\
    SET_PIN_AS_OUTPUT (2, 9);						\
    SET_PIN_AS_OUTPUT (2, 10);						\
    SET_PIN_AS_OUTPUT (2, 11);						\
    CLEAR_GPIO_PIN (2, 0);						\
    CLEAR_GPIO_PIN (2, 1);						\
    CLEAR_GPIO_PIN (2, 2);						\
    CLEAR_GPIO_PIN (2, 3);						\
    CLEAR_GPIO_PIN (2, 4);						\
    CLEAR_GPIO_PIN (2, 5);						\
    CLEAR_GPIO_PIN (2, 6);						\
    CLEAR_GPIO_PIN (2, 7);						\
    CLEAR_GPIO_PIN (2, 8);						\
    CLEAR_GPIO_PIN (2, 9);						\
    CLEAR_GPIO_PIN (2, 10);						\
    CLEAR_GPIO_PIN (2, 11);						\
    REG_AT_INDEX (LPC_IOCON_BASE, IOCON_OFFSET_2_0) = MODE_FLOAT;	\
    REG_AT_INDEX (LPC_IOCON_BASE, IOCON_OFFSET_2_1) = MODE_FLOAT;	\
    REG_AT_INDEX (LPC_IOCON_BASE, IOCON_OFFSET_2_2) = MODE_FLOAT;	\
    REG_AT_INDEX (LPC_IOCON_BASE, IOCON_OFFSET_2_3) = MODE_FLOAT;	\
    REG_AT_INDEX (LPC_IOCON_BASE, IOCON_OFFSET_2_4) = MODE_FLOAT;	\
    REG_AT_INDEX (LPC_IOCON_BASE, IOCON_OFFSET_2_5) = MODE_FLOAT;	\
    REG_AT_INDEX (LPC_IOCON_BASE, IOCON_OFFSET_2_6) = MODE_FLOAT;	\
    REG_AT_INDEX (LPC_IOCON_BASE, IOCON_OFFSET_2_7) = MODE_FLOAT;	\
    REG_AT_INDEX (LPC_IOCON_BASE, IOCON_OFFSET_2_8) = MODE_FLOAT;	\
    REG_AT_INDEX (LPC_IOCON_BASE, IOCON_OFFSET_2_9) = MODE_FLOAT;	\
    REG_AT_INDEX (LPC_IOCON_BASE, IOCON_OFFSET_2_10) = MODE_FLOAT;	\
    REG_AT_INDEX (LPC_IOCON_BASE, IOCON_OFFSET_2_11) = MODE_FLOAT;	\
    SET_PIN_AS_OUTPUT (2, 0);						\
    SET_PIN_AS_OUTPUT (2, 1);						\
    SET_PIN_AS_OUTPUT (2, 2);						\
    SET_PIN_AS_OUTPUT (2, 3);						\
    SET_PIN_AS_OUTPUT (2, 4);						\
    SET_PIN_AS_OUTPUT (2, 5);						\
    CLEAR_GPIO_PIN (2, 0);						\
    CLEAR_GPIO_PIN (2, 1);						\
    CLEAR_GPIO_PIN (2, 2);						\
    CLEAR_GPIO_PIN (2, 3);						\
    CLEAR_GPIO_PIN (2, 4);						\
    CLEAR_GPIO_PIN (2, 5);						\
    REG_AT_INDEX (LPC_IOCON_BASE, IOCON_OFFSET_2_0) = MODE_FLOAT;	\
    REG_AT_INDEX (LPC_IOCON_BASE, IOCON_OFFSET_2_1) = MODE_FLOAT;	\
    REG_AT_INDEX (LPC_IOCON_BASE, IOCON_OFFSET_2_2) = MODE_FLOAT;	\
    REG_AT_INDEX (LPC_IOCON_BASE, IOCON_OFFSET_2_3) = MODE_FLOAT;	\
    REG_AT_INDEX (LPC_IOCON_BASE, IOCON_OFFSET_2_4) = MODE_FLOAT;	\
    REG_AT_INDEX (LPC_IOCON_BASE, IOCON_OFFSET_2_5) = MODE_FLOAT;	\
    REG_AT_INDEX (LPC_SYSCTL_BASE, SYSAHBCLKCTRL_INDEX) = 0x1f;	\
  } while (0)

// Port is by default port #3.  Pins 3.0, 3.1 and 3.2 are easily
// identifiable and easily accessible on the LPC1115 LPCxpresso eval board.
#define BEEBS_PORT			3
#define LED_PORT			0
#define LED_PIN				7

// Provide a set of macros to do the pin toggling /////////////////////////
// Initialise the pin + clocks.  One GPIO port occupies an address range of 0x10000 bytes.
#define PIN_INIT(number)                \
    do {                                				\
        /* Turn on GPIO Clock */        				\
        REG_AT_INDEX (LPC_SYSCTL_BASE, SYSAHBCLKCTRL_INDEX) |= 1<<6;	\
        /* Set pin direction to output.  */				\
	SET_PIN_AS_OUTPUT (BEEBS_PORT, (number));			\
        /* Select function 0 (GPIO) and mode 0 (floating) for pin */	\
        REG_AT_INDEX (LPC_IOCON_BASE, PIN_IOCON_OFFSET (number)) = 0;	\
        /* Clear pin value.  */						\
        CLEAR_GPIO_PIN (BEEBS_PORT, (number));				\
    } while(0)

#define LED_INIT() \
	do {									\
        /* Turn on GPIO Clock */        					\
        REG_AT_INDEX (LPC_SYSCTL_BASE, SYSAHBCLKCTRL_INDEX) |= 1<<6;		\
        /* Select function 0 (GPIO) and mode (1 << 3) (pulldown) for pin */	\
        REG_AT_INDEX (LPC_IOCON_BASE, IOCON_OFFSET_LED) = (0x1 << 3); 		\
        /* Set pin direction to output.  */					\
        SET_PORT_AS_OUTPUT (LED_PORT, LED_PIN); 				\
        /* Clear pin value.  */							\
        CLEAR_GPIO_PIN (LED_PORT, LED_PIN);					\
    } while(0)

// Set the pin to high.  One GPIO port occupies an address range of 0x10000 bytes.
#define PIN_SET(number)                 \
    do {                                \
        /* Set pin NUMBER of port BEEBS_PORT. */ \
        REG_AT_INDEX (LPC_GPIO_BASE + BEEBS_PORT * 0x10000, 1 << (number)) = 1UL << (number);      \
    } while(0)

// Set the pin to low.  One GPIO port occupies an address range of 0x10000 bytes.
#define PIN_CLEAR(number)               \
    do {                                \
        /* Clear pin NUMBER of port BEEBS_PORT.  */  \
        CLEAR_GPIO_PIN (BEEBS_PORT, (number));       \
    } while(0)

// Turn the LED ON.
#define LED_SET(number)                 \
    do {                                \
        /* Set pin 7 of port 0. */ \
        REG_AT_INDEX (LPC_GPIO_BASE + LED_PORT * 0x10000, 1 << LED_PIN) = 1UL << LED_PIN;      \
    } while(0)

// Turn the LED OFF.
#define LED_CLEAR(number)               \
    do {                                \
        /* Clear pin 7 of port 0. */ \
        REG_AT_INDEX (LPC_GPIO_BASE + LED_PORT * 0x10000, 1 << LED_PIN) = 0UL << LED_PIN;      \
    } while(0)

#endif /* CHIPSUPPORT_H */
