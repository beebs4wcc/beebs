/* Copyright (C) 2014-2019 Embecosm Limited and University of Bristol

   Contributor James Pallister <james.pallister@bristol.ac.uk>

   This file is part of the Bristol/Embecosm Embedded Benchmark Suite.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include <string.h>
#include "support.h"
#include <libopencm3/stm32/f0/rcc.h>
#include <libopencm3/stm32/f0/flash.h>

// This file is needed to copy the initialised data from flash to RAM

extern unsigned char __data_start__;
extern unsigned char __data_end__;
extern unsigned char _data_loadaddr;

extern unsigned char __bss_start__;
extern unsigned char __bss_end__;

void software_init_hook()
{
    memcpy(&__data_start__, &_data_loadaddr, 
        (unsigned)&__data_end__ - (unsigned)&__data_start__);
}

/* The values below come from STM document en.DM00031936.pdf
   (DocID018940 Rev 9), section 3.2.1, pages 54-57.  */

/* Default clock frequency after reset: 8 MHz from HSI.  */
uint32_t chip_clock_frequency = 8000000;

/* Default prefetch enable setting after reset: 1 (ON).  */
uint32_t chip_prefetch_enable = 1;

/* Default wait state of the Flash memory after reset: 0.
   Range of valid values is 0..7 (3 bits).  */
uint32_t chip_flash_ws = 0;

/* Wait until a SYSCLK source switch becomes effective.  CLK is the new SYSCLK source.
   Use a busy loop, do not care about energy here. */
void chip_wait_for_sysclk_status(enum rcc_osc clk)
{
    uint32_t clk_sws_mask;

    /* Skip the wait for clocks that cannot be sources of SYSCLK.
       For possible SYSCLK sources select appropriate mask to check the SWS field.  */
    switch (clk)
    {
        /* SYSCLK cannot be derived from these clocks.  */
        case RCC_LSE:
        case RCC_LSI:
        case RCC_HSI14:
            return;

        /* These are the only clocks visible in SWS field of RCC_CFGR.  */
        case RCC_HSE:
            clk_sws_mask = RCC_CFGR_SWS_HSE;
            break;
        case RCC_HSI:
            clk_sws_mask = RCC_CFGR_SWS_HSI;
            break;
        case RCC_PLL:
            clk_sws_mask = RCC_CFGR_SWS_PLL;
            break;
        case RCC_HSI48:
            clk_sws_mask = RCC_CFGR_SWS_HSI48;
            break;
        default:
            /* Assume we wait for HSI (the default source after reset).  */
            clk_sws_mask = RCC_CFGR_SWS_HSI;
            break;
    }

    /* Wait util the SYSCLK source status matches the requested clock.  */
    while ((RCC_CFGR & RCC_CFGR_SWS) != clk_sws_mask);
}

/* Stop the PLL.  This is NOT done in the rcc_osc_off() code which skips the
   stopping of the PLL.  Return non-zero value if PLL cannot be stopped (e.g.,
   if it is used as the current SYSCLK.)
   The procedure is described in STM document en.DM00031936.pdf (DocID018940 Rev 9),
   section 6.2.4, page 102.  */
uint32_t chip_pll_off(void)
{
    /* Skip stopping if SYSCLK is currently driven from PLL.  */
    if ((RCC_CFGR & RCC_CFGR_SWS) == RCC_CFGR_SWS_PLL)
        return 1;

    /* Disable the PLL.  */
    RCC_CR &= ~RCC_CR_PLLON;

    /* Wait until PLL is effectively off.  */
    while (RCC_CR & RCC_CR_PLLRDY) ;
    return 0;
}

/* Set chip timing configuration:
   - frequency
   - Flash prefetch
   - Flash wait states.

   Note: F05x only has one possible HSI-based sourcce with
   PREDIV == 2, but HSE can be used for divisions of 8 MHz by 1..16.
   Multipliers are in the range 2..16, giving two possible sets
   of frequencies not exceeding 48 MHz:
    - HSI: 8 MHz / 2 * 2..12
    - HSE: 8 MHz / (1..16) * (2..16), result <= 48 MHz.  */
void chip_set_timings(uint32_t freq, uint32_t use_prefetch, uint32_t flash_ws)
{
    /* Use values of chip_clock_freq, chip_prefetch_enable and chip_flash_ws
       to set up the clock controls.  For frequency, use closest LOWER-or-equal
       combination, switching to HSE if frequencies under 8 MHz are required.  */

    /* Freq must be clamped to 48 MHz.  */
    if (freq > MAX_SYSCLK_FREQ)
        freq = MAX_SYSCLK_FREQ;

    /* Set MCO (cloock output).
       The MCO can be made available outside the chip as an alternate function of pin PA8.

       NOTE: On F05x there is no divider for MCO, only direct output is available.  */
    RCC_CFGR = (RCC_CFGR & ~(RCC_CFGR_MCO_MASK << RCC_CFGR_MCO_SHIFT)) | (RCC_CFGR_MCO_SYSCLK << RCC_CFGR_MCO_SHIFT);

    /* Freqs below 8 MHz require HSE, i.e., the external clock.  */
    if (freq < HSI_FREQ)
    {
        /* On the f0discovery board, HSE is Not Mounted.  */
        /* FORNOW do nothing.  */
    }
    else /* 8 MHz and above: use HSI/2 as clock source.  */
    {
        /* To be on the safe side, set multiplier to the floor of the frequency ratio.  */
        uint32_t pll_multiplier = freq / (HSI_FREQ / 2);

        /* Turn HSI on and wait until it stabilises.  */
        rcc_osc_on(RCC_HSI);
	    rcc_wait_for_osc_ready(RCC_HSI);

        /* Switch to HSI oscillator.  Wait until SYSCLK has switched to HSI
           before disabling PLL.  */
	    rcc_set_sysclk_source(RCC_HSI);
        chip_wait_for_sysclk_status(RCC_HSI);

        /* Use unscaled SYSCLK as HCLK and PCLK.  */
	    rcc_set_hpre(RCC_CFGR_HPRE_NODIV);
	    rcc_set_ppre(RCC_CFGR_PPRE_NODIV);

        /* Set prefetch according to user requirement while running
           SYSCLK on HSI.
           Non-zero value means 'enable'.  */
	    if (use_prefetch)
            flash_prefetch_enable();
        else
            flash_prefetch_disable();

        /* Set the wait state according to user requirement
           instead of the minimal required value (at freqs > 24 Mhz
           this can put the Flash read timings off-spec.)
           Do this while running SYSCLK on HSI.  */
	    flash_set_ws(flash_ws & 0x7);

        /* Stop the PLL to change PLL parameters.  Restart the PLL and set it as
           SYSCLK source once the PLL params were set, then stop HSI and update
           book-keeping variables.
           Use own PLL stopping code as rcc_osc_off() does NOTHING on F0 if CLK is PLL.  */
        if (chip_pll_off() == 0)
        {
	        /* Multiplier values are encoded with a downward offset of 2:
               multiplier 2 is represented by code 0.
               The F0 implementation of rcc_set_pll_multiplication_factor expects as arg
               the shifted bitmask of the adjusted value.  */
	        rcc_set_pll_source(RCC_CFGR_PLLSRC_HSI_CLK_DIV2);
	        rcc_set_pll_multiplication_factor((pll_multiplier - 2) << RCC_CFGR_PLLMUL_SHIFT);

            /* Enable PLL and let it stabilise.  */
	        rcc_osc_on(RCC_PLL);
	        rcc_wait_for_osc_ready(RCC_PLL);

            /*  Set PLL as SYSCLK source and wait until SYSCLK has switched to PLL.  */
	        rcc_set_sysclk_source(RCC_PLL);
            chip_wait_for_sysclk_status(RCC_PLL);

            /* Turn off HSI.  */
            rcc_osc_off(RCC_HSI);

            /* Update book-keeping variables.  */
	        rcc_apb1_frequency = (HSI_FREQ / 2) * pll_multiplier;
	        rcc_ahb_frequency = (HSI_FREQ / 2) * pll_multiplier;
        }
    }
}
