/* Common main.c for the benchmarks

   Copyright (C) 2014 Embecosm Limited and University of Bristol
   Copyright (C) 2018 Embecosm Limited

   Contributor: James Pallister <james.pallister@bristol.ac.uk>
   Contributor: Jeremy Bennett <jeremy.bennett@embecosm.com>

   This file is part of the Bristol/Embecosm Embedded Benchmark Suite.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

   SPDX-License-Identifier: GPL-3.0-or-later */

#ifndef WCC
#include "support.h"
#include <stdlib.h>
#else
/* Configure the required attribute for benchmark execution section.  */
#ifdef RAMEXEC_SECTION
/* FORNOW use a constant name.  */
#define EXEC_SECTION_ATTRIBUTE __attribute__((section(".ramexecute")))
#else
/* No RAM-execute section specified.  */
#define EXEC_SECTION_ATTRIBUTE
#endif
#endif

#ifndef REPEAT_FACTOR
#define REPEAT_FACTOR 4096
#endif

/* For Thumbulator we need only two iterations. */
#ifdef THUMBULATOR
#undef REPEAT_FACTOR
#define REPEAT_FACTOR 4096
#endif

#define VALUE_TO_STRING(x) #x
#define VALUE(x) VALUE_TO_STRING(x)
#define VAR_NAME_VALUE(var) #var " = " VALUE(var)

#ifndef WCC
#pragma message VAR_NAME_VALUE(REPEAT_FACTOR)

extern int initialise_benchmark (void);
extern int verify_benchmark (int unused);
#else
extern void initialise_benchmark (void);
extern int verify_benchmark (int);
extern int benchmark (void);
extern int verify_benchmark (int);
extern void initialise_board (void);
extern int verify_board (void);
extern void start_trigger (void);
extern void stop_trigger (void);
extern void exit (int);
#endif

EXEC_SECTION_ATTRIBUTE
int
#ifndef WCC
main (int   argc __attribute__ ((unused)),
      char *argv[] __attribute__ ((unused)) )
#else
main (int   argc, char *argv[] )
#endif
{
  int i;
  volatile int result;
  int correct;

  initialise_board ();
  initialise_benchmark ();
  start_trigger ();

  for (i = 0; i < REPEAT_FACTOR; i++)
    {
      initialise_benchmark ();
      result = benchmark ();
    }

  stop_trigger ();

  /* bmarks that use arrays will check a global array rather than int result */

  correct = verify_benchmark (result);

  exit (!correct);
#ifdef WCC
  return 0;
#endif

}	/* main () */

/*
   Local Variables:
   mode: C++
   c-file-style: "gnu"
   End:
*/
