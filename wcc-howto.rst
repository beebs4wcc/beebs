#########
WCC HOWTO
#########

Describes the WCC-build process that targets the ARM Nucleo and Discovery
-boards, and the Thumbulator simulator. The overall process is to first build
the code libraries using Gcc, which are then linked into the final benchmark
binaries by WCC. The benchmark code itself is compiled with WCC.


Prepare the repo
================

Make sure there is a arm-none-eabi-gcc (tested with GCC 7.3.1) installed in your environment.
The repository is assumed to reside in *rootDir*.

Initialize and get the source for the libopencm3 submodule:

.. code-block:: bash

   git submodule init
   git submodule update

Build the libopencm3 libraries:

.. code-block:: bash

   cd <rootDir>/tools/libopencm3
   make

Build target support libraries
==============================

Configure BEEBS benchmark according to the intended target.

.. code-block:: bash

   ./configure --host=arm-none-eabi --with-chip=<chip-config> \
         --with-board=<board-config> CFLAGS='-nostartfiles'
   make


...where **<chip-config>** can be one of the following:

.. code-block:: bash

  stm32f051
  stm32f051-thumbulator

...and **<board-config>** one of the following:

.. code-block:: bash

  stm32f0discovery
  stm32f0discovery-thumbulator

If building for...

* ... the Discovery- or Nucleo-board, choose configs **without** *-thumbulator*
* ... the Thumbulator, choose **with** *-thumbulator*


Build benchmarks with WCC
=========================

Each benchmark can be build by invoking *make* in the benchmark directory:

.. code-block:: bash

   make -f Makefile.wcc

By default this will build the binaries for the Discovery board.

To build for the Nucleo board, we need to inform WCC about the
*cortexm0-nucleo.mem* memory-layout in the repository root. For this, create
the file **.wccrc** in your homefolder ('~/') with the following line (adjust
path accordingly):

.. code-block:: bash

   MEMORY_LAYOUT_DESCRIPTION : path/to/repository/cortexm0-nucleo.mem

When building for the Thumbulator, modify the src/Makefile.wcc.common to enable the cflag *-DTHUMBULATOR*.

After the build(s) are done, each benchmark directory will have:

* CodeVariants/ - Directory with all the SPM-allocated binary variants
* CodeVariantsfile.csv - Describes whether each basic block is allocated (1) or not (0) into SPM for each variant.
* AllocationFile.csv - Same as before but describes the SPM allocation in form of a vector.

.. note::

   There is a convenience script *wcc-build.sh* in the root directory for
   compiling (and cleaning) multiple benchmarks at once.


