/* Chip support for ARM stm32f051.

   Copyright (C) 2014 Embecosm Limited and the University of Bristol

   Contributor James Pallister <james.pallister@bristol.ac.uk>

   This file is part of BEEBS

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the Free
   Software Foundation; either version 3 of the License, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.  */

#ifndef CHIPSUPPORT_H
#define CHIPSUPPORT_H

/* Define the chip family prior to including toplevel headers.  */
#define STM32F0
#include <libopencm3/cm3/common.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/flash.h>

/* Performance and energy configuration */

/* Absolute maximum frequency: 48MHz.  */
#define MAX_SYSCLK_FREQ 48000000

/* HSI clock frequency of the STM32F051.  */
#define HSI_FREQ 8000000

/* Default clock frequency after reset: 8 MHz */
extern uint32_t chip_clock_frequency;

/* Default prefetch enable setting after reset: 1 (ON).  */
extern uint32_t chip_prefetch_enable;

/* Default wait state of the Flash memory after reset: 0.
   Range of valid values is 0..7 (3 bits).  */
extern uint32_t chip_flash_ws;

// Provide macros to do the pin toggling ////////////////////////////////////

// Initialise the pin + clocks
#define PIN_INIT(number)                \
    do {                                \
        /* Turn on GPIO C Clock */      \
        RCC_AHBENR |= 1<<19;            \
        /* Turn on GPIO C */            \
        GPIOC_MODER |= 1 << number;     \
        /* Pull low GPIO C */           \
        GPIOC_BSRR |= 1 << (number+16); \
    } while(0)

// Set the pin to high
#define PIN_SET(number)                 \
    do {                                \
        /* Pull low GPIO C */           \
        GPIOC_BSRR |= 1 << number;      \
    } while(0)

// Set the pin to low
#define PIN_CLEAR(number)               \
    do {                                \
        /* Pull low GPIO C */           \
        GPIOC_BSRR |= 1 << (number+16); \
    } while(0)

#endif /* CHIPSUPPORT_H */

/* Chip timings setup hook */
extern void chip_set_timings(uint32_t frequency, uint32_t use_prefetch, uint32_t wait_states);
